# Strongkeep API client

WIP: This library is under development.

See [TODO](TODO.md) for the list of the things done/left to implement.

Supports:

* Ruby 2.4
* Strongkeep API v1.(dev)


## Getting access to Strongkeep API

Before you can access Strongkeep API, you have to generate a client key pair and register its public key with Strongkeep.

### Generating your client key

`strongkeep-ruby` comes with a CLI tool which can be used to generate new and inspect existing keys in Strongkeep Key Format. To use the tool you should install the gem first:

```
> gem i strongkeep
```

Then list available commands:
```
> strongkeep h
Commands:
  strongkeep generate          # Generate a new key
  strongkeep help [COMMAND]    # Describe available commands or one specific command
  strongkeep inspect FILENAME  # Inspect a key in Strongkeep format
  strongkeep version           # Show version
```

To generate a new key, use `generate` command, optionally providing a label for the new key:
```
> strongkeep g -l "My Key to the Kingdom"
     label: "My Key to the Kingdom"
randomness: /dev/urandom
   private:
{"type":"ed25519","label":"My Key to the Kingdom","prv_key":"6e65d05498d8800e5dbfce89c1336474b802b6043c3193a7c05f683fe58a6f7c"}

    public:
{"type":"ed25519","label":"My Key to the Kingdom","pub_key":"784e70cde1121a90170af21bea18b0eb5b157faca1627ffa09d3a88fec2ea280"}
```

Here you see that the result is two keys in Strongkeep Key Format (the JSON) - private and public. You keep private part secret and use it to configure the API client, you register the public part with Strongkeep, so that it recognizes you as a valid client.

### DIY key generation

TODO: how to construct a valid ed25519 key pair in Strongkeep Key Format without using the software provided by Strongkeep.

## Usage

### Configuring your client

Provided your client key (private) is stored in `client-key.json`:

```ruby
require 'strongkeep'

client_key = File.read('client-key.json')
strongkeep = Strongkeep::Client.new(mode: :test, client_key: client_key)
```

### API available?

To test if the client is configured correctly, API is accessible and it does recognize your client key you can invoke `#available?` method:

```ruby
require 'strongkeep'

client_key = File.read('client-key.json')
strongkeep = Strongkeep::Client.new(mode: :test, client_key: client_key)
if strongkeep.available?
  puts 'Everything is fine'
else
  puts 'Nope :('
end
```


### Resources and Entities

In `strongkeep-ruby` library the Strongkeep API resources are represented by `Strongkeep:Resource` class or its descendant and they act as repositories (see Repository or Data Mapper Patterns). Resources provide methods to query API and fetch individual Entities, as well as to create new or persist updated Entities.

Entities are in turn just simple data objects and are not expected to contain any logic.

The configured `Strongkeep::Client` offers a list of methods that return resources:
```
#assets
#accounts
```

Any resource is an instance of a Class inheriting from `Strongkeep::Resource` and offers the following methods:
```
#all
#each_page
#first
#find(id)
```

These methods return an instance of `Strongkeep::Resource::Entity`, which provides a method-like access to object properties:
```ruby
...
strongkeep.assets.first
# => {"id"=>"1f7b169c846f218ab552fa82fbf86758",
#  "currency"=>"BTC",
#  "description"=>"Bitcoin",
#  "exponent"=>8,
#  "features"=>{"early_confirmation"=>true}}

strongkeep.assets.first.id # => "1f7b169c846f218ab552fa82fbf86758"
```

#### Longer Example:

```ruby
require 'strongkeep'

client_key = File.read('client-key.json')
strongkeep = Strongkeep::Client.new(mode: :test, client_key: client_key)

# construct a map of asset.id -> asset.currency
assets = strongkeep.assets.all.map { |a| [a.id, a.currency] }.to_h

# list balances
strongkeep.accounts.all.each do |account|
  puts "#{assets[account.asset_id]}: #{account.balance}"
end
```

### Assets resource: /api/v1/assets

Assets are accessed through `#assets` method of the client object:
```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.assets # => a resource, <Strongkeep::Assets>
```

#### List all supported assets

You can use `#all` method on `Strongkeep::Assets` instance to list all the assets provided by Strongkeep:

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.assets.all.each do |asset|
  puts "#{asset.id}: (#{asset.currency}) #{asset.description}"
end
# produces:
# 00000000000000000000000000000001: (BTC) Bitcoin
# 00000000000000000000000000000002: (LTC) Litecoin
# 00000000000000000000000000000003: (ETH) Ethereum
# 00000000000000000000000000000004: (BCH) Bitcoin Cash
# 00000000000000000000000000000005: (ETC) Ethereum Classic
```

#### Use stable IDs to reference assets
> IMPORTANT:
>
> Any asset supported by Strongkeep gets a **unique** and **immutable** ID. For example Bitcoin has an ID of `00000000000000000000000000000001`. Using asset IDs is the only recommended way of referencing assets in `strongkeep-ruby` as well as in Strongkeep API, the other ways are not considered stable.
>
> For example, using `currency` code (e.g. `BTC`) may be tempting, but in the rapidly changing world of cryptocurrencies it's not guaranteed to stay the same (see [`XBT`](https://en.bitcoin.it/wiki/Bitcoin_symbol#Currency_code)), or even be unique (see `BCC`, Bitcoin Cash vs BitConnect).
>
> Using `description` is even less appropriate for automated processing. It is expected for the description to be updated every so often, as cryptocurrencies update, evolve, fork and so on.
>
> See: ...
> TODO: link to API documentation section on supported assets

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.assets['00000000000000000000000000000001'] # this is Bitcoin
# => <Strongkeep::Assets::Reference>
# pointing to: /api/v1/assets/00000000000000000000000000000001
```

#### Validating cryptocurrency addresses

For all the assets supported by Strongkeep an asset resource reference returned by `#assets[<asset_id>]` has a `#validate_address()` method that is specific to the selected asset (cryptocurrency).

The address validators are smart enough to not only recognize particular cryptocurrency, but also the client's mode of operation: `live` or `test`, which also affects the correctness of tested addresses.

For example in Bitcoin case, the address validator will determine whether *mainnet* or *testnet3* addresses should be seen as correct.

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.mode # => :test
strongkeep.assets['00000000000000000000000000000001'].validate_address(
  'mk3cMZny48oZnVhqnonm5AvUtg7X33kA6z' # valid testnet3 address
) # => true

strongkeep.assets['00000000000000000000000000000001'].validate_address(
  '14wShKtaYXvTJtH57NugP8gn3cdjAfW5a8' # valid mainnet address
) # => false, this address is not valid because client is in :test mode
```

##### Simplified address validation

Although it is recommended to use proper address validation provided by `strongkeep-ruby`, it is possible to use simplified Regexp-based validation as an additional preliminary validation step (e.g. on the Frontend side).

Asset entities retrieved from Strongkeep API have `address_pattern` attribute containing asset/cryptocurrency specific regular expression:

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.assets.first
# => {"id"=>"00000000000000000000000000000001",
#  "currency"=>"BTC",
#  "description"=>"Bitcoin",
#  "exponent"=>8,
#  "address_pattern"=>"^bc1[a-zA-HJ-NP-Z0-9]{25,39}$|^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$",
#  "features"=>{"early_confirmation"=>true}}

```

Note that it also depends on the client's mode of operation and should only applicable in this mode.

### Accounts resource: /api/v1/accounts

Accounts are accessed through `#accounts` method of the client object:

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.accounts # => a resource, <Strongkeep::Accounts>
```

#### List all accounts

You can use `#all` method on `Strongkeep::Accounts` instance to list all the accounts you have on Strongkeep platform:

```ruby
...
strongkeep # => <Strongkeep::Client>
# construct a map of asset.id -> asset.currency
assets = strongkeep.assets.all.map { |a| [a.id, a.currency] }.to_h

# list balances
strongkeep.accounts.all.each do |account|
  puts "#{assets[account.asset_id]}: #{account.balance}"
end
# produces:
# BTC: 831.15832828
# LTC: 387.55012060
# ETH: 688.900054580570895268
# BCH: 48.44778903
# ETC: 539.753291227931867979
```

### Account addresses resource: /api/v1/accounts/{account_id}/addresses

Any account on Strongkeep platform has a list of deposit addresses linked with it.
These are represented by a resource returned by `#addresses` method:

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.accounts['1f7b169c846f218ab552fa82fbf86758'].addresses.first
# => {"id"=>"c97d2d2a313e4f95957818a7b3edca49",
#  "address"=>"bc1nu3yh6827enjdjg80j6p77gpnkyjh6g55wa8m",
#  "created_at"=>"2018-05-18T20:38:05Z",
#  "reference"=>"Invoice 153891"}
```

#### Create a new deposit address

You can request a new deposit address to be created for an account, and provide an arbitrary reference string to integrate a new address with your internal system (e.g. you can put a customer an order ID in the reference, and the newly created address stays associated with it):

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.accounts['1f7b169c846f218ab552fa82fbf86758'].addresses.create(
  reference: 'order:65136'
)
# => {"id"=>"b913fc65de4b625772db251d6c734cf0",
#  "address"=>"bc1629jhy9fhlazktdnewqle4r9nlwz36ds945l7xggqaw67",
#  "created_at"=>"2018-06-08T15:08:04Z",
#  "reference"=>"order:65136"}
```

### Account transactions resource: /api/v1/accounts/{account_id}/txs

Account transactions are a resource accessible via `#txs` method through an account resource reference:

```ruby
...
strongkeep # => <Strongkeep::Client>
strongkeep.accounts['1f7b169c846f218ab552fa82fbf86758'].txs.count # => 5
```


### Account transactions: request a withdrawal

TODO: document this

```ruby
...
strongkeep # => <Strongkeep::Client>
uid = strongkeep.random_uid # => 16 bytes of random data, hex encoded
strongkeep.accounts['1f7b169c846f218ab552fa82fbf86758'].txs[uid].create(
  ...
) 
```


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/strongkeep-ruby. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Strongkeep::Ruby project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/strongkeep-ruby/blob/master/CODE_OF_CONDUCT.md).
