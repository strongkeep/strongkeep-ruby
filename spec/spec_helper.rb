require 'bundler/setup'
require 'simplecov'
SimpleCov.start do
  add_filter "/spec/"
end

require 'strongkeep'

require 'support/vcr'
require 'support/fixtures'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    #
    # Set up a new connection mode: 'rspec'
    #
    Strongkeep::MODES[:rspec] = {
      content_type: 'application/json',
      user_agent: "strongkeep-ruby/#{Strongkeep::VERSION} (rspec)",
      api_base_url: 'https://rspec.api_base_url.foobar/api/v1/',
      strongkeep_pub_keys: [
        '{"type":"ed25519","label":"Strongkeep rspec @ 2018-06-01","pub_key":"4f64de16c422635d7a8ad4e51e49987153c9c705671e0c7487b6df16ecf74d80"}'
      ]
    }
  end
  config.include RSpecFixturesHelper
end
