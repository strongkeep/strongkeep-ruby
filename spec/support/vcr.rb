require 'webmock'
require 'vcr'

VCR.configure do |config|
  config.ignore_localhost = false
  config.hook_into :webmock
  config.cassette_library_dir = 'spec/cassettes'
  config.configure_rspec_metadata!
  config.allow_http_connections_when_no_cassette = false
  config.default_cassette_options = {
    allow_playback_repeats: true,
    serialize_with: :json,
    record: :once,
    match_requests_on: %i[method uri body],
    allow_unused_http_interactions: true
  }
end
