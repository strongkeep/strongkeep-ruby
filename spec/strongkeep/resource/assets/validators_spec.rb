RSpec.describe 'Assets address validators' do
  let(:client_key_json) { '{"type":"ed25519","label":"Client Christopher","prv_key":"e112a3a1c9cdd0d4ae22091674d1dded8fcb289ea77c38a963f3efd49b1a8934"}' }
  let(:strongkeep_test)      { Strongkeep::Client.new(mode: :rspec, client_key: client_key_json) }
  let(:strongkeep_live)      { Strongkeep::Client.new(mode: :live, client_key: client_key_json) }
  let(:asset_id_btc)    { '00000000000000000000000000000001' }

  context 'BTC, testnet' do
    let(:asset_id) { asset_id_btc }
    subject { strongkeep_test.assets[asset_id] }

    it 'correctly validates P2PKH addresses' do
      expect(subject.validate_address('mk3cMZny48oZnVhqnonm5AvUtg7X33kA6z')).to be true
      expect(subject.validate_address('14wShKtaYXvTJtH57NugP8gn3cdjAfW5a8')).to be false
    end

    it 'correctly validates P2SH addresses' do
      expect(subject.validate_address('2N3Lr1eWogwCqjdJwi9ELhqKRjxVfDLNbrA')).to be true
      expect(subject.validate_address('33PeEPorYHupkBM7uRBdeUKdocYozxRiY1')).to be false
    end
  end # BTC, testnet

  context 'BTC, mainnet' do
    let(:asset_id) { asset_id_btc }
    subject { strongkeep_live.assets[asset_id] }

    it 'correctly validates a P2PKH addresses' do
      expect(subject.validate_address('mk3cMZny48oZnVhqnonm5AvUtg7X33kA6z')).to be false
      expect(subject.validate_address('14wShKtaYXvTJtH57NugP8gn3cdjAfW5a8')).to be true
    end

    it 'correctly validates P2SH addresses' do
      expect(subject.validate_address('2N3Lr1eWogwCqjdJwi9ELhqKRjxVfDLNbrA')).to be false
      expect(subject.validate_address('33PeEPorYHupkBM7uRBdeUKdocYozxRiY1')).to be true
    end
  end # BTC, mainnet
end