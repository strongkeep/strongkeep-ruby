RSpec.describe Strongkeep::Assets do
  let(:client_key_json) { '{"type":"ed25519","label":"Client Christopher","prv_key":"e112a3a1c9cdd0d4ae22091674d1dded8fcb289ea77c38a963f3efd49b1a8934"}' }
  let(:strongkeep)      { Strongkeep::Client.new(mode: :rspec, client_key: client_key_json) }
  let(:asset_id_btc)    { '00000000000000000000000000000001' }

  subject { strongkeep.assets }

  it 'is provided by Strongkeep::Client' do
    expect(strongkeep).to respond_to(:assets)
  end

  it { is_expected.to respond_to(:[]) }
  it { is_expected.to respond_to(:all) }
  it { is_expected.to respond_to(:first) }
  it { is_expected.to respond_to(:count) }

  it 'has a Reference' do
    expect(subject[asset_id_btc]).to be_a Strongkeep::Assets::Reference
  end
end