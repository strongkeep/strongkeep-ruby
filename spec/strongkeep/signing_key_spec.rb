RSpec.describe Strongkeep::SigningKey do
  let(:label)   { 'test-key' }
  let(:random_prv_key) { SecureRandom.hex(32) }
  let(:json_key_prv) { '{"type":"ed25519","label":"Client Christopher","prv_key":"e112a3a1c9cdd0d4ae22091674d1dded8fcb289ea77c38a963f3efd49b1a8934"}' }
  let(:json_key_pub) { '{"type":"ed25519","label":"Client Christopher","pub_key":"7f17dd9c9afef559929f959750d49ab1194a5d51a77f4cd8207875994cb79175"}' }
  let(:test_message) { 'I am a banana' }
  let(:test_message_valid_signature) { '38378c6af9dc3e5e6ec59355af0535b808d58eb166321d023239027a005cd56830f04990767a79aa72de767569916d65c9c82575446eda6f6eb6b5dc39626f07' }
  let(:test_message_invalid_signature) { '83e49184ec3c47a9698b9724d975ccd80b53f989468c6c7664172a4240f5c62e6ee78808c232dcb4685e5a97b539de4a3fe5aefd33aeda33c8d184ca5d9c4107' }

  subject { described_class.new(label, random_prv_key, nil) }

  it 'constructs a SigningKey from a set of valid parameters' do
    expect { subject }.to_not raise_error
    expect(subject).to be_a Strongkeep::SigningKey
  end

  it { is_expected.to respond_to(:h2b) }
  it { is_expected.to respond_to(:b2h) }
  it { is_expected.to respond_to(:to_s) }
  it { is_expected.to respond_to(:to_public) }
  it { is_expected.to respond_to(:to_json) }
  it { is_expected.to respond_to(:sign) }
  it { is_expected.to respond_to(:verify) }
  it { is_expected.to respond_to(:verify!) }

  # class methods
  it { expect(described_class).to respond_to(:generate_random_key) }
  it { expect(described_class).to respond_to(:from_json) }

  it 'produces a public key out of private' do
    expect(subject.private?).to be true
    expect { subject.to_public }.to_not raise_error
    expect(subject.to_public).to be_a Strongkeep::SigningKey
    expect(subject.to_public.private?).to be_falsey
  end

  context '.from_json' do
    subject(:subject_private) { described_class.from_json(json_key_prv) }
    subject(:subject_public) { described_class.from_json(json_key_pub) }

    it 'successfully deserializes a valid private key' do
      expect { subject_private }.to_not raise_error
      expect(subject_private).to be_a Strongkeep::SigningKey
      expect(subject_private.private?).to be true
    end

    it 'successfully deserializes a valid public key' do
      expect { subject_public }.to_not raise_error
      expect(subject_public).to be_a Strongkeep::SigningKey
      expect(subject_public.private?).to be_falsey
    end
  end # .from_json

  context '#sign' do
    let(:signing_key) { described_class.from_json(json_key_prv) }
    subject { signing_key.sign(test_message) }

    it 'produces a correct signature' do
      expect { subject }.to_not raise_error
      expect(subject).to be_a String
      expect(subject).to eq test_message_valid_signature
    end
  end # #sign

  context '#verify' do
    let(:verifying_key) { described_class.from_json(json_key_pub) }
    subject(:subject_valid) { verifying_key.verify(test_message, test_message_valid_signature) }
    subject(:subject_invalid) { verifying_key.verify(test_message, test_message_invalid_signature) }

    it 'correctly validates a valid signature' do
      expect { subject_valid }.to_not raise_error
      expect(subject_valid).to be true
    end

    it 'correctly validates an invalid signature' do
      expect { subject_invalid }.to_not raise_error
      expect(subject_invalid).to be_falsey
    end
  end # #verify

  context '#verify!' do
    let(:verifying_key) { described_class.from_json(json_key_pub) }
    subject(:subject_valid) { verifying_key.verify!(test_message, test_message_valid_signature) }
    subject(:subject_invalid) { verifying_key.verify!(test_message, test_message_invalid_signature) }

    it 'correctly validates a valid signature' do
      expect { subject_valid }.to_not raise_error
      expect(subject_valid).to be true
    end

    it 'correctly validates an invalid signature' do
      expect { subject_invalid }.to raise_error(Strongkeep::Errors::InvalidSignature)
    end
  end # #verify!
end # describe Strongkeep::SigningKey
