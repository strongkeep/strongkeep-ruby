require 'spec_helper'

RSpec.describe 'Account txes creation' do
  let(:client) do
    Strongkeep::Client.new(mode: :rspec, client_key: fixture('strongkeep-rspec-key.json'))
  end

  let(:withdrawal) do
    client.accounts[account.id].txs.create_withdrawal attributes
  end

  let(:full_attributes) do
    {
      address: 'mk3cMZny48oZnVhqnonm5AvUtg7X33kA6z',
      amount: '1.0',
      tx_id: '1234567890abcdef1234567890abcdef'
    }
  end

  let(:attributes) do
    full_attributes
  end

  context 'when receiver is LTC', vcr: true do
    let(:ltc_asset_id) do
      '00000000000000000000000000000002'
    end

    let(:account) do
      client.accounts.all.detect { |acc| acc.asset_id == ltc_asset_id }
    end

    let(:receiver_currency) { 'LTC' }

    it 'creates withdrawal' do
      new_record = withdrawal
      expect(new_record.to_h).to include(
        "created_at" => "2018-01-03 18:15:00 UTC",
        "id" => "7b04311a95b94723ad319ff718c3eacd",
        "receiver" => {"amount"=>"0.999"},
        "revision" => 2,
        "sender" => {"amount"=>"1"},
        "state" => "confirmed",
        "state_reason" => {"code"=>2002, "message"=>"confirmed by block 000000000000000000085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f"},
        "type" => "withdrawal",
        "updated_at" => "2018-01-03 18:15:05 UTC"
      )
    end

    context 'when address is missing' do
      let(:attributes) do
        full_attributes.delete_if { |k, _| k == :address }
      end

      it 'raises validation error' do
        expect { withdrawal }.to raise_error('Address is missing')
      end
    end

    context 'when address is invalid by response' do
      it 'raises request error' do
        expect { withdrawal }.to raise_error(Strongkeep::Errors::BadRequest)
      end
    end

    context 'when amount is missing' do
      let(:attributes) do
        full_attributes.delete_if { |k, _| k == :amount }
      end

      it 'raises validation error' do
        expect { withdrawal }.to raise_error('Amount is missing')
      end
    end

    context 'when tx_id is missing' do
      let(:attributes) do
        full_attributes.delete_if { |k, _| k == :tx_id }
      end

      it 'raises validation error' do
        expect { withdrawal }.to raise_error('Tx ID is missing')
      end
    end

    context 'when tx_id is bad' do
      let(:attributes) do
        full_attributes.merge!(tx_id: '!not_just_32 hex digits')
      end

      it 'raises validation error' do
        expect { withdrawal }.to raise_error('Tx ID is not valid')
      end
    end
  end
end
