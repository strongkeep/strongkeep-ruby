require 'spec_helper'

RSpec.describe 'Callback verification' do
  let(:client) do
    Strongkeep::Client.new(mode: :rspec, client_key: fixture('strongkeep-rspec-key.json'))
  end

  let(:request_body) do
    '{"id":"1e5daa5a738e64a70a839ace504c1d84","type":"deposit","revision":1,"state":"confirmed","state_reason":{"code":2,"message":"Transaction confirmed in block 00000000000000000079b8da5f491aee0f0ac1266f54e4ab54605449f79e2f61"},"created_at":"2018-05-07T12:08:51Z","updated_at":"2018-05-30T07:40:33Z","sender":{"amount":"75.65854929","external_tx":{"txid":"5c82ee1a62d62693419265e3b97d2a3ab2219fbf3553e5b07f3e3d818297a917","to_address":{"id":"9e3223ab439e73270ce54c3125a76987","address":"bc1ht4vn0e9qzvyj3wvu55rmykj","created_at":"2018-05-08T20:17:03Z","reference":"Invoice 120556"}}},"receiver":{"amount":"75.50753422","account_id":"7faae7e0f0ee788a1fbf694f0f687a52"}}'
  end

  let(:headers) do
    {
      'Ignored' => 'Test value',
      'X-Strongkeep-Callback-Id' => '4765d2f1e601fe275f9cd321c96a32d0',
      'X-Strongkeep-Location' => '/api/v1/accounts/7faae7e0f0ee788a1fbf694f0f687a52/txs/1e5daa5a738e64a70a839ace504c1d84',
      'X-Strongkeep-Signature' => '5ac51b267d9f30b310a8f38d8e85183ee04de5576c5eae0d402308b4c43381ec78234528f0ad1d8654d0887c40618678d111afdea3fbcb6b1e13faf753c60b02',
      'Content-Type' => 'application/json; charset=UTF-8'
    }
  end

  let(:public_key) do
    'd190132dcb3776eca4997d7a8c196fd8dff9218438299cfe41214c77411abc37'
  end

  context 'when public_key is correct' do
    it 'returns callback if signature is right' do
      expect(Strongkeep::Callback.parse(request_body, headers, public_key).id).to eq '1e5daa5a738e64a70a839ace504c1d84'
    end

    it 'raises error false if signature is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Signature' => 'not_ok'), public_key) }.to raise_error('expected 64 byte signature, got 3')
    end

    it 'raises error if callback id is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Callback-Id' => 'not_ok'), public_key) }.to raise_error('Untrusted callback')
    end

    it 'raises error if location is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Location' => 'not_ok'), public_key) }.to raise_error('Untrusted callback')
    end
  end

  context 'when public_key is wrong' do
    let(:public_key) do
      '5bc1b22f1c9230d7dd2dfb1440c3fa3f41fb6cb71be8839822f07ae7d952b3a4'
    end

    it 'raises error if signature is right' do
      expect { Strongkeep::Callback.parse(request_body, headers, public_key) }.to raise_error('Untrusted callback')
    end

    it 'raises error if signature is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Signature' => 'not_ok'), public_key) }.to raise_error('expected 64 byte signature, got 3')
    end

    it 'raises error if callback id is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Callback-Id' => 'not_ok'), public_key) }.to raise_error('Untrusted callback')
    end

    it 'raises error if location is not ok' do
      expect { Strongkeep::Callback.parse(request_body, headers.merge('X-Strongkeep-Location' => 'not_ok'), public_key) }.to raise_error('Untrusted callback')
    end
  end
end
