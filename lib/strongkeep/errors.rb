# frozen_string_literal: true

module Strongkeep
  module Errors
    # Constructs an anonymous error class which inherits from base_error
    #
    # @param base_error [Class < StandardError]
    # @param default_message [String]
    # @return [Class < StandardError]
    #
    def self.construct_error_class(base_error, default_message)
      klass = Class.new(base_error) do
        define_method :initialize do |name = default_message|
          super(name)
        end
      end
      klass
    end

    InvalidSignature = construct_error_class(StandardError, 'Invalid signature')

    ConnectionError = construct_error_class(StandardError, 'Connection level error')

    # HTTP 4xxx
    ClientError          = construct_error_class(ConnectionError, 'HTTP client error')
    BadRequest           = construct_error_class(ClientError, 'Bad HTTP request')
    Forbidden            = construct_error_class(ClientError, 'Forbidden')
    NotFound             = construct_error_class(ClientError, 'Resource not found')
    UnsupportedMediaType = construct_error_class(ClientError, 'Unsupported media type')

    # HTTP 5xx
    ServerError = construct_error_class(ConnectionError, 'HTTP server error')

    InternalServerError = construct_error_class(ServerError, 'Internal server error')

    # Resources and Entities
    InvalidEntity = construct_error_class(StandardError, 'Invalid entity')
  end # module Errors
end # module Strongkeep
