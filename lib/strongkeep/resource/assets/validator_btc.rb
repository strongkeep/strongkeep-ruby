# frozen_string_literal: true

require 'bitcoin'

module Strongkeep
  class Assets
    class ValidatorBTC < Strongkeep::Assets::Validator
      def validate_address(address)
        Strongkeep.exclusively_with(Bitcoin) do
          Bitcoin.network = live? ? :bitcoin : :testnet3
          return Bitcoin.valid_address?(address)
        end
      end
    end # class ValidatorBTC
  end # class Assets
end # module Strongkeep
