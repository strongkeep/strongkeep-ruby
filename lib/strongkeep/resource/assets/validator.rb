# frozen_string_literal: true

module Strongkeep
  class Assets
    #
    # Validator serves as a base class for asset/cryptocurrency specific address validators.
    # Ideally there should be a specific Validator-based class for each cryptocurrency.
    #
    # For assets/cryptocurrencies not having a specific Validator-based class,
    # there is ValidatorDefault, which does a simplified regexp based validation.
    #
    class Validator
      attr_reader :asset_ref

      # Instantiates a Validator object
      #
      # @param asset_ref [Strongkeep::Assets::Reference]
      #
      def initialize(asset_ref)
        @asset_ref = asset_ref
      end

      # Returns the current client
      #
      # @param [Strongkeep::Client]
      #
      def client
        asset_ref.resource.client
      end

      # Returns the mode with which the client is configured
      #
      # @return [Symbol] :live, :test etc
      def mode
        client.mode
      end

      # Returns true if the client is in live mode
      #
      # @return [true,false]
      #
      def live?
        client.live?
      end

      # Validates address
      #
      # Should be overloaded in the descendant classes
      #
      # @param address [String]
      # @return [true,false]
      #
      def validate_address(_address)
        raise "#validate_address not implemented in #{self.class}"
      end
    end # class Validator
  end # class Assets
end # module Strongkeep
