# frozen_string_literal: true

module Strongkeep
  class Assets
    class Reference < Strongkeep::Resource::Reference
      ASSET_VALIDATOR_MAP = {
        # NOTE: asset ids are expected to be immutable
        # id -> validator_class
        '00000000000000000000000000000001' => Strongkeep::Assets::ValidatorBTC, # btc
        '00000000000000000000000000000002' => Strongkeep::Assets::ValidatorLTC, # ltc
        '00000000000000000000000000000003' => Strongkeep::Assets::ValidatorETH, # eth
        '00000000000000000000000000000004' => Strongkeep::Assets::ValidatorBCH, # bch
        '00000000000000000000000000000005' => Strongkeep::Assets::ValidatorETH, # etc
      }.freeze

      #
      # Validates an address based on cryptocurrency and client mode
      #
      # @param address [String]
      # @return [true,false]
      #
      def validate_address(address)
        validator_class.new(self).validate_address(address)
      end

      # Returns a validator class based on asset/cryptocurrency
      #
      # @return [Class < Strongkeep::Assets::Validator]
      #
      def validator_class
        ASSET_VALIDATOR_MAP[id] || Strongkeep::Assets::ValidatorDefault
      end
    end # class Reference
  end # class Assets
end # module Strongkeep
