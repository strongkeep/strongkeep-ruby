# frozen_string_literal: true

module Strongkeep
  class Assets
    #
    # A simplified regexp-based Validator
    #
    # ValidatorDefault performs a call to fetch the asset entity from the API using current client,
    # so it cannot be used offline.
    #
    class ValidatorDefault < Strongkeep::Assets::Validator
      VALIDATION_REGEXP_KEY = 'address_pattern'

      def validate_address(address)
        asset = asset_ref.fetch
        unless asset.key?(VALIDATION_REGEXP_KEY)
          raise 'Unsupported feature: ValidatorDefault expects the underlying asset to have' \
            " '#{VALIDATION_REGEXP_KEY}' property"
        end
        re = Regexp.new(asset[VALIDATION_REGEXP_KEY])
        !re.match(address).nil?
      end
    end # class ValidatorBCH
  end # class Assets
end # module Strongkeep
