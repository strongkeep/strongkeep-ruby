# frozen_string_literal: true

module Strongkeep
  class Assets
    class ValidatorETH < Strongkeep::Assets::Validator
      ETH_MAINNET_REGEXP = /^0x[a-fA-F0-9]{40}$/
      ETH_TESTNET_REGEXP = /^0x[a-fA-F0-9]{40}$/

      # TODO: Consider adding capitals-based checksum validation:
      # https://github.com/ethereum/EIPs/blob/master/EIPS/eip-55.md
      #
      def validate_address(address)
        re = live? ? ETH_MAINNET_REGEXP : ETH_TESTNET_REGEXP
        !re.match(address).nil?
      end
    end # class ValidatorETH
  end # class Assets
end # module Strongkeep
