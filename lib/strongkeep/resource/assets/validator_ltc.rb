# frozen_string_literal: true

require 'bitcoin'

module Strongkeep
  class Assets
    class ValidatorLTC < Strongkeep::Assets::Validator
      def validate_address(address)
        Strongkeep.exclusively_with(Bitcoin) do
          Bitcoin.network = live? ? :litecoin : :litecoin_testnet
          return Bitcoin.valid_address?(address)
        end
      end
    end # class ValidatorLTC
  end # class Assets
end # module Strongkeep
