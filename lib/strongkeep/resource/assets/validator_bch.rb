# frozen_string_literal: true

require_relative 'validator_btc'

module Strongkeep
  class Assets
    class ValidatorBCH < Strongkeep::Assets::ValidatorBTC
      def validate_address(address)
        # run ValidatorBTC
        super
      end
    end # class ValidatorBCH
  end # class Assets
end # module Strongkeep
