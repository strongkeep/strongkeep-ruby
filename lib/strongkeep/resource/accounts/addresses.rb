# frozen_string_literal: true

module Strongkeep
  class Accounts
    class Addresses < Strongkeep::Resource
      ALLOWED_ATTRIBUTES_ON_CREATE = %w[
        reference early_confirmation
      ].freeze

      resource_name 'addresses'

      def initialize(client, path)
        self.class.resource_path(path)
        super(client)
      end

      # Creates a new deposit address for the selected account
      #
      # @see POST /api/v1/accounts/{account_id}/addresses
      #
      # @param params [Hash] parameters for the new address
      # @option param [String,nil] :reference an arbitrary string that will be associated with new address
      # @return [Strongkeep::Resource::Entity]
      #
      def create(params = {})
        super
      end

      # Before create, filter allowed attributes
      #
      def before_create(entity)
        entity.delete_if { |k, _| !ALLOWED_ATTRIBUTES_ON_CREATE.include?(k.to_s) }
        super
      end
    end # class Addresses
  end # class Accounts
end # module Strongkeep
