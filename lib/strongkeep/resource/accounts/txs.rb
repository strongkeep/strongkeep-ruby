# frozen_string_literal: true

module Strongkeep
  class Accounts
    class Txs < Strongkeep::Resource
      ALLOWED_ATTRIBUTES_ON_CREATE = %w[address amount tx_id].freeze
      BODY_ATTRIBUTES = %w[address amount].freeze
      TX_ID_REGEXP = /\A[a-f0-9]{32}\Z/
      resource_name 'txs'

      def initialize(client, path)
        self.class.resource_path(path)
        super(client)
      end

      # Creates a new withdrawal from the selected account
      #
      # @see POST /api/v1/accounts/{account_id}/txes/{tx_id}/withdrawal
      #
      # @param params [Hash] parameters for the new transaction
      # @return [Strongkeep::Resource::Entity]
      #
      def create_withdrawal(params = {})
        entity = entity_class.coerce(params)
        before_validate(entity)
        validate(entity)
        before_create(entity)
        withdrawal_path = "#{path}/#{CGI::escape(entity.tx_id)}/withdrawal"
        body = entity.to_h.delete_if { |k,_| !BODY_ATTRIBUTES.include?(k) }
        results = client.connection.post(withdrawal_path, body)
        entity_class.new(results)
      end

      # A hook to be run before create
      #
      def before_create(entity)
        entity.delete_if { |k, _| !ALLOWED_ATTRIBUTES_ON_CREATE.include?(k.to_s) }
        super
      end

      # Validates an entity before persisting, raises an error on validation errors
      #
      # @param entity [Strongkeep::Resource::Entity]
      #
      # @raise [Strongkeep::Errors::InvalidEntity]
      #
      def validate(entity)
        raise Strongkeep::Errors::InvalidEntity.new('Address is missing') unless entity.address
        raise Strongkeep::Errors::InvalidEntity.new('Amount is missing') unless entity.amount
        raise Strongkeep::Errors::InvalidEntity.new('Tx ID is missing') unless entity.tx_id
        raise Strongkeep::Errors::InvalidEntity.new('Tx ID is not valid') unless TX_ID_REGEXP.match?(entity.tx_id)
        super
      end

    end # class Txs
  end # class Accounts
end # module Strongkeep
