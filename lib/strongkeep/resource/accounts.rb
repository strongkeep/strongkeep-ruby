# frozen_string_literal: true

module Strongkeep
  class Accounts < Resource
    resource_name 'accounts'

    def reference_class
      Reference
    end

    # A reference to an account entity
    #
    class Reference < Strongkeep::Resource::Reference
      # Returns a transactions resource
      #
      # @return [Strongkeep::Accounts::Txs]
      #
      def txs
        @txs ||= Strongkeep::Accounts::Txs.new(resource.client, path.join('txs'))
      end

      # Returns an addresses resource
      #
      # @return [Strongkeep::Accounts::Addresses]
      #
      def addresses
        @addresses ||= Strongkeep::Accounts::Addresses.new(resource.client, path.join('addresses'))
      end
    end # class Reference
  end # class Accounts
end # module Strongkeep

require_relative 'accounts/txs'
require_relative 'accounts/addresses'
