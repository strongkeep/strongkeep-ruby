# frozen_string_literal: true

module Strongkeep
  #
  # Represents assets resource:
  # /api/v1/assets
  #
  class Assets < Resource
    resource_name 'assets'

    def reference_class
      Reference
    end
  end # class Assets
end # module Strongkeep

require_relative 'assets/validator'
require_relative 'assets/validator_default'
require_relative 'assets/validator_btc'
require_relative 'assets/validator_bch'
require_relative 'assets/validator_ltc'
require_relative 'assets/validator_eth'
require_relative 'assets/reference'
