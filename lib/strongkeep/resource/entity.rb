# frozen_string_literal: true

require 'hashie'

module Strongkeep
  class Resource
    class Entity < Hashie::Mash
      def self.resource=(resource)
        @resource = resource
      end

      def self.resource
        @resource
      end

      # Simple string representation
      #
      def to_s
        "<Strongkeep::Resource::Entity (#{self.class.resource.name})>"
      end

      # Coerce a Hash or other Entity value into an instance of this class
      #
      # @param value [Hash,Strongkeep::Resource::Entity]
      # @return [Strongkeep::Resource::Entity]
      #
      def self.coerce(value)
        return valie if value.is_a?(self)
        new(value)
      end
    end # class Entity
  end # class Resource
end # module Strongkeep
