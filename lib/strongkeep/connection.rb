# frozen_string_literal: true

require 'http'

module Strongkeep
  class Connection
    include Strongkeep::Errors

    attr_reader :client, :params

    def initialize(client, params = {})
      @client = client
      @params = params
    end

    # Returns the API base URL the connection is configured with
    #
    # @return [URI]
    #
    def api_base_url
      URI(params[:api_base_url])
    end

    # Returns the content type the connection is configured with
    #
    # @return [String]
    #
    def content_type
      params[:content_type]
    end

    # Returns the user agent the connection is configured with
    #
    # @return [String]
    #
    def user_agent
      params[:user_agent]
    end

    # Performs a GET request to an API endpoint
    #
    # @param path [String] an API endpoint to query, without the base part of the path
    # @param params [Hash] a set of params to pass as query string
    #
    # @return [String]
    #
    def get(path, data = {})
      raise ArgumentError, 'Hash is expected as request data' unless data.is_a?(Hash)
      encoded_data = URI.encode_www_form(data)
      request(:get, path, encoded_data)
    end

    # Performs a POST request to an API endpoint
    #
    # @param path [String] an API endpoint to send request to, without the base part of the path
    # @param params [Hash] a set of params to pass as query string
    #
    # @return [String]
    #
    def post(path, data = {})
      raise ArgumentError, 'Hash is expected as request data' unless data.is_a?(Hash)
      encoded_data = data.to_json
      request(:post, path, encoded_data)
    end

    private

    # Sends a request to the API and returns parsed response
    #
    # @param method [:get,:post]
    # @param path [String] a relative (without leading '/') path to the API endpoint
    # @param encoded_data [String] a URI- or JSON encoded data, depending on the method
    # @return [Hash] the response parsed from JSON
    #
    def request(method, path, encoded_data)
      if path.to_s.start_with?('/')
        raise ArgumentError, 'Relative path is expected, absolute is given'
      end
      api_base_path = Pathname.new(api_base_url.path)
      api_absolute_path = api_base_path.join(path.to_s)
      url = URI.join(api_base_url, api_absolute_path.to_s)
      client.logger.debug "Requesting #{method} #{path}: #{url}"
      params = {}
      http = HTTP.headers(construct_api_headers(api_absolute_path.to_s, encoded_data))
      if method == :get
        url.query = encoded_data unless encoded_data.empty?
      else
        http = http.headers('Content-Type' => content_type)
        params[:body] = encoded_data
      end
      params[:ssl_context] = ssl_context
      client.logger.debug "> #{method.to_s.upcase}: #{url}"
      headers = http.default_options.to_hash[:headers]
      headers.each do |k, v|
        client.logger.debug "  #{k}: #{v}"
      end
      response = http.send(method, url, params)
      client.logger.debug "< #{response.code} #{response.reason}"
      begin
        body = JSON.parse(response.body)
      rescue JSON::ParserError => e
        raise ConnectionError, "Failed to parse response: #{e}"
      end
      return body if (200...300).cover?(response.status)
      respond_with_error(response.code, body['message'])
    rescue StandardError => e
      client.logger.warn "#{method.to_s.upcase}: #{url}, failed with error: #{e}"
      raise
    end

    # Map unsuccessful HTTP response to appropriate exception and raise it
    #
    def respond_with_error(status, message)
      case status
      when 400
        raise BadRequest, message
      when 403
        raise Forbidden, message
      when 404
        raise NotFound, message
      when 415
        raise UnsupportedMediaType, message
      when 500
        raise InternalServerError, message
      when 400...500
        raise ClientError, message
      when 500...600
        raise ServerError, message
      else
        raise ConnectionError, message
      end
    end

    # Returns timestamp based nonce to a microsecond resolution
    #
    # @return [Integer]
    #
    def self.nonce
      (Time.now.to_f * 1_000_000).to_i
    end

    # Constructs a standard set of headers required by the API,
    # including signature of the request
    #
    # @return [Hash]
    #
    def construct_api_headers(absolute_path, encoded_data)
      nonce = self.class.nonce
      signature = sign_request(absolute_path, nonce, encoded_data)

      {
        'User-Agent' => user_agent,
        'Accept' => content_type,
        'X-Strongkeep-Customer-Key' => client.client_key.pub_key,
        'X-Strongkeep-Nonce' => nonce,
        'X-Strongkeep-Signature' => signature
      }
    end

    # Calculates request signature
    #
    # @param absolute_path [String]
    # @param nonce [Integer,String]
    # @param request_data [String]
    #
    # @return [String] a hex encoded signature
    #
    def sign_request(absolute_path, nonce, request_data)
      msg = absolute_path + '|' + nonce.to_s + '|' + request_data
      signature = client.sign_message(msg)
      client.logger.debug 'sign_request: ' \
        "path=#{absolute_path}, nonce=#{nonce}, msg=#{msg.inspect}, signature=#{signature}"
      signature
    end

    # Returns configured SSLContext
    #
    def ssl_context
      return @ssl_context if @ssl_context
      @ssl_context = OpenSSL::SSL::SSLContext.new
      @ssl_context.verify_mode = OpenSSL::SSL::VERIFY_PEER
      cert_store = OpenSSL::X509::Store.new
      cert_store.set_default_paths
      @ssl_context.cert_store = cert_store
      @ssl_context
    end
  end # class Connection
end # module Strongkeep
