# frozen_string_literal: true

module Strongkeep
  #
  # Strongkeep::Callback object represents a callback data received from Strongkeep API
  #
  class Callback < Hashie::Mash
    X_LOCATION_HEADER = 'X_STRONGKEEP_LOCATION'
    X_CALLBACK_ID_HEADER = 'X_STRONGKEEP_CALLBACK_ID'
    X_SIGNATURE_HEADER = 'X_STRONGKEEP_SIGNATURE'

    # Checks callback requests details and extracts trusted object.
    # @raises ArgumentError if callback cannot be trusted
    #
    def self.parse(body, headers, public_key)
      if trusted_callback?(body, headers, public_key)
        return new(JSON.parse(body))
      else
        raise ArgumentError.new('Untrusted callback')
      end
    end

    # Checks if callback http request is from trusted source
    # @param body [String] UTF-8 body of callback request
    # @param headers [Hash] Application headers (starting with X)
    # @param public_key [String] Strongkeep public key (different for each environment)
    # @see https://gitlab.com/strongkeep/api#callbacks
    #
    # @returns [Boolean] true if callback signature is matching
    #
    def self.trusted_callback?(body, headers, public_key)
      key = Strongkeep::SigningKey.new(nil, nil, public_key)
      normalized_headers = {}
      headers.each_pair do |k, v|
        normalized_headers[k.gsub('-', '_').upcase] = v
      end
      location = normalized_headers[X_LOCATION_HEADER]
      callback_id = normalized_headers[X_CALLBACK_ID_HEADER]
      signature = normalized_headers[X_SIGNATURE_HEADER]
      key.verify("#{location}|#{callback_id}|#{body}", signature)
    end

  end # class Callback
end # module Strongkeep
