# frozen_string_literal: true

require 'ed25519'
require 'securerandom'
require 'json'

module Strongkeep
  #
  # Strongkeep::SigningKey represents a signing (private) or verifying (public) key
  # for a Strongkeep's digital signature algorithm of choice: ed25519
  #
  # @see
  #   * https://ed25519.cr.yp.to/
  #   * https://en.wikipedia.org/wiki/EdDSA
  #   * https://github.com/crypto-rb/ed25519
  #
  class SigningKey
    STRONGKEEP_KEY_TYPE = 'ed25519'
    PRV_KEY_BYTE_SIZE   = 32
    PRV_KEY_HEX_SIZE    = PRV_KEY_BYTE_SIZE * 2
    PRV_KEY_HEX_REGEXP  = /[a-f0-9]{#{PRV_KEY_HEX_SIZE}}/
    PUB_KEY_BYTE_SIZE   = 32
    PUB_KEY_HEX_SIZE    = PUB_KEY_BYTE_SIZE * 2
    PUB_KEY_HEX_REGEXP  = /[a-f0-9]{#{PUB_KEY_HEX_SIZE}}/

    include Strongkeep::Utils
    extend  Strongkeep::Utils

    attr_reader :label, :prv_key, :pub_key

    # @param label [String] arbitrary key label
    # @param prv_key [String,nil] a private key (seed) of 32 bytes as a hex encoded string
    # @param pub_key [String,nil] a public key of 32 bytes as a hex encoded string
    #
    def initialize(label, prv_key, pub_key)
      if prv_key && (!prv_key.is_a?(String) || prv_key !~ PRV_KEY_HEX_REGEXP)
        raise ArgumentError, "A hex encoded string of #{PRV_KEY_BYTE_SIZE} bytes is expected as prv_key"
      end
      if pub_key && (!pub_key.is_a?(String) || pub_key !~ PUB_KEY_HEX_REGEXP)
        raise ArgumentError, 'A hex encoded string of #{PUB_KEY_BYTE_SIZE} bytes is expected as pub_key'
      end
      if prv_key.nil? && pub_key.nil?
        raise ArgumentError, 'At least one of prv_key, pub_key is expected'
      end
      @label   = label.dup
      @prv_key = prv_key&.dup
      @pub_key = prv_key ? derive_pub_key : pub_key
    end

    # Returns true if the key contains the private part and can produce signatures
    #
    def private?
      !prv_key.nil?
    end

    # Converts the key instance (private or public) to a public key, containing only the public part
    #
    # @return [Strongkeep::SigningKey]
    #
    def to_public
      self.class.new(label, nil, pub_key)
    end

    # Signs a message
    #
    # @param message [String]
    # @return [String] a hex encoded signature
    #
    def sign(message)
      raise 'Failed to sign message, a private key is required' unless private?
      sk = ::Ed25519::SigningKey.new(h2b(prv_key))
      b2h(sk.sign(message))
    end

    # Verifies message signature using public key
    #
    # @param message [String]
    # @param signature [String] a hex encoded signature
    # @return [true,false]
    #
    def verify(message, signature)
      vk = ::Ed25519::VerifyKey.new(h2b(pub_key))
      vk.verify(h2b(signature), message)
      true
    rescue ::Ed25519::VerifyError => _
      false
    end

    # Verifies message signature using public key, raises error if signature is invalid
    #
    # @param message [String]
    # @param signature [String] a hex encoded signature
    # @return [true]
    # @raise [Strongkeep::Errors::InvalidSignature]
    #
    def verify!(message, signature)
      verify(message, signature) || (raise Strongkeep::Errors::InvalidSignature)
    end

    # Returns JSON representation in Strongkeep Key Format
    #
    # @return [String]
    #
    def to_json
      h = { type: STRONGKEEP_KEY_TYPE, label: label }
      if private?
        h[:prv_key] = prv_key
      else
        h[:pub_key] = pub_key
      end
      h.to_json
    end

    # Simple string representation, does NOT expose secrets
    #
    # @return [String]
    #
    def to_s
      "<#{self.class.name}:#{label.inspect}:#{private? ? 'prv' : 'pub'}>"
    end

    # Generates a random signing (private) key using standard Ruby source of randomness
    #
    # @param label [String]
    # @return [Strongkeep::SigningKey]
    #
    def self.generate_random_key(label)
      prv_key = SecureRandom.hex(PRV_KEY_BYTE_SIZE)
      new(label, prv_key, nil)
    end

    # Generates a key using an external source of randomness
    #
    # @param label [String]
    # @param randomness [String] a filename to read random data from
    # @return [Strongkeep::SigningKey]
    #
    def self.generate_key(label, randomness)
      prv_key_bytes = File.open(randomness) { |f| f.read(PRV_KEY_BYTE_SIZE) }
      new(label, b2h(prv_key_bytes), nil)
    end

    # Reads and parses JSON string containing the key in Strongkeep Key Format
    #
    # @param json_key [String]
    # @return [Strongkeep::SigningKey]
    #
    def self.from_json(json_key)
      hash = JSON.parse(json_key)
      type = hash['type']
      raise ArgumentError, "Invalid key type: #{type}" unless type == STRONGKEEP_KEY_TYPE
      new(hash['label'], hash['prv_key'], hash['pub_key'])
    rescue StandardError => e
      raise "Failed to load key from JSON: #{e}"
    end

    private

    # Derives a public key from the prv_key
    #
    # @return [String] a hex encoded public key
    #
    def derive_pub_key
      sk = ::Ed25519::SigningKey.new(h2b(prv_key))
      b2h(sk.verify_key.to_bytes)
    end
  end # class SigningKey
end # module Strongkeep
