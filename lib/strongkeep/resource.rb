# frozen_string_literal: true

require 'pathname'

module Strongkeep
  #
  # Strongkeep::Resource represents an abstract resource provided by the API.
  #
  # E.g. 'assets', 'accounts' etc.
  #
  class Resource
    attr_reader :client

    # DSL: Gets or sets the name of the resource
    #
    # @param value [String]
    # @return [String] current name of the resource
    #
    def self.resource_name(value = nil)
      @resource_name = value if value
      @resource_name
    end

    # DSL: Gets or sets the API path of the resource
    #
    # By default, if path is not set, it is the same as the name.
    #
    # @param value [String,Pathname]
    # @return [Pathname] current path of the resource
    #
    def self.resource_path(value = nil)
      @resource_path = Pathname.new(value) if value
      @resource_path || Pathname.new(resource_name)
    end

    # Creates an instance of Resource using the given client
    #
    # @param client [Strongkeep::Client]
    #
    def initialize(client)
      @client = client
    end

    # Returns a reference to a Resource entity identified by ID
    #
    # For example, if `assets` is a Resource that is served by /api/v1/assets,
    # `assets["165fe943"]` is a reference to /api/v1/assets/165fe943
    #
    # @param id [String] identifier of an entity
    # @return [Strongkeep::Resource::Reference]
    #
    def [](id)
      reference_class.new(self, id)
    end

    # Iterates over Resource collection, fetching all entities page by page
    #
    # Loads entities page by page and yields the `Array<Strongkeep::Resource::Entity>`
    # to a given block.
    #
    # It is recommended to use #each_page for large collections, #all for small collections.
    #
    # @param params [Hash] pagination params
    # @option params [Integer] :per_page number of items loaded per single page
    #
    # Example:
    #   strongkeep.accounts[''].txs.each_page(per_page: 100) do |txs|
    #     txs # => <Array<Strongkeep::Resource::Entity>>
    #   end
    #
    def each_page(params = {}, &_block)
      params = params[:per_page] ? { per_page: params[:per_page] } : {} # allow only :per_page
      page = 1
      loop do
        results = client.connection.get(path, params.merge(page: page))
        break if results[name].nil? || results[name].empty?
        pagination = results['pagination'] || {}
        entities = results[name].map { |e| entity_class.new(e) }
        yield entities
        page += 1
        break if page > pagination['page_count']
      end
    end

    # Returns a list of all entities
    #
    # Loads all entities of a Resource page by page and returns them in a single list.
    # Note that for a very large collection this call will take lots of time.
    #
    # It is recommended to use #all only for collections with low element count, e.g. `assets`.
    # @see #each_page
    #
    # @return [Array<Strongkeep::Entity>]
    #
    def all
      entities = []
      each_page { |e| entities += e }
      entities
    end

    # Returns a single entity from a resource
    #
    # @return [Strongkeep::Entity]
    #
    def first
      results = client.connection.get(path, page: 1, per_page: 1)
      results[name] && entity_class.new(results[name].first)
    end

    # Finds an entity with a given ID
    #
    # @param id [String]
    # @return [Strongkeep::Entity]
    # @raise [Strongkeep::Errors::NotFound]
    #
    def find(id)
      result = client.connection.get(path_to(id))
      entity_class.new(result)
    end

    # Returns the number of entities
    #
    # @return [Integer]
    #
    def count
      results = client.connection.get(path, page: 1, per_page: 1)
      results['pagination'] && results['pagination']['total_count']
    end

    # Returns the resource name
    #
    # @return [String]
    #
    def name
      self.class.resource_name
    end

    # Returns the resource path
    #
    # @return [Pathname]
    #
    def path
      self.class.resource_path
    end

    # Returns the reference under the resource path
    #
    # @pasram id [String]
    # @return [Pathname]
    #
    def path_to(id)
      path.join(id.to_s)
    end

    # Simple string representation
    #
    def to_s
      "<#{self.class} (#{self.class.name})>"
    end

    # A hook to be run before create
    #
    def before_create(_entity); end

    # A hook to be run before update
    #
    def before_update(_entity); end

    # A hook to be run before validation
    #
    def before_validate(_entity); end

    # Validates an entity before persisting, raises an error on validation errors
    #
    # @param entity [Strongkeep::Resource::Entity]
    #
    # @raise [Strongkeep::Errors::InvalidEntity]
    #
    def validate(_entity); end

    # Creates a new entity
    #
    # @param entity [Hash,Strongkeep::Resource::Entity]
    # @return [Strongkeep::Resource::Entity]
    #
    def create(entity)
      raise "Method #create is not exposed in class #{self.class}" unless method_exposed?(:create)
      entity = entity_class.coerce(entity)
      before_validate(entity)
      validate(entity)
      before_create(entity)
      results = client.connection.post(path, entity.to_h)
      entity_class.new(results)
    end

    # Updates an existing entity
    #
    # @param entity [Hash,Strongkeep::Resource::Entity]
    # @return [Strongkeep::Resource::Entity]
    #
    def update(entity)
      raise "Method #update is not exposed in class #{self.class}" unless method_exposed?(:update)
      entity = entity_class.coerce(entity)
      before_validate(entity)
      validate(entity)
      before_update(entity)
      raise 'Not implemented'
      # TODO: currently no API resource supports updating, implement when needed
      # results = client.connection.post(path.join(entity.id), entity.to_h)
      # entity_class.new(results)
    end

    # Returns entity class associated with this resource
    #
    # @return [Class < Strongkeep::Resource::Entity]
    #
    def entity_class
      return @entity_class if @entity_class
      @entity_class = Class.new(Strongkeep::Resource::Entity)
      @entity_class.resource = self
      @entity_class
    end

    # Returns the reference class
    #
    # Can be redefined in Resource descendants
    #
    # @return [Class <= Strongkeep::Resource::Reference]
    #
    def reference_class
      Reference
    end

    private

    # Returns true if the method is exposed in the end (descendant class)
    #
    # @param method_name [String,Symbol]
    # @return [true,false]
    #
    def method_exposed?(method_name)
      self.class.instance_methods(false).include?(method_name.to_sym)
    end

    # Represents a reference to a single Resource entity
    #
    # Sometimes you want to reference not a Resource as a collection, but a particular entity
    # of this Resource. Strongkeep::Resource::Reference is exactly for that:
    #
    # Example:
    #   # a resource
    #   strongkeep.assets
    #   # points to: /api/v1/assets
    #
    #   # identifies the entity, without loading it
    #   strongkeep.assets['1f7b169c846f218ab552fa82fbf86758']
    #   # points to: /api/v1/assets/1f7b169c846f218ab552fa82fbf86758
    #
    # A Reference allows you to perform actions on the particular entity without loading it.
    # For example, if you want to access some account's transactions:
    #   # an account transactions resource
    #   strongkeep.accounts['7faae7e0f0ee788a1fbf694f0f687a52'].txs
    #   # points to: /api/v1/accounts/7faae7e0f0ee788a1fbf694f0f687a52/txs
    #
    class Reference
      attr_reader :resource, :id

      def initialize(resource, id)
        @resource = resource
        @id = id
      end

      # Loads the referenced entity
      #
      # @return [Strongkeep::Resource::Entity]
      #
      def fetch
        resource.find(id)
      end

      # Returns a path to the referenced entity
      #
      # @return [Pathname]
      #
      def path
        resource.path_to(id)
      end
    end # class Reference
  end # class Resource
end # module Strongkeep

require_relative 'resource/assets'
require_relative 'resource/accounts'
require_relative 'resource/entity'
