# frozen_string_literal: true

module Strongkeep
  #
  # Strongkeep::Client object represents a configured connection to the Strongkeep API
  #
  class Client
    attr_reader :mode
    attr_reader :client_key
    attr_reader :logger
    attr_reader :connection

    # Creates a configured Client instance
    #
    # @param params [Hash]
    # @option params [String,Symbol] :mode one of "test" or "live"
    # @option params [String] :client_key a JSON encoded client *private* key
    # @option params [Logger,nil] :logger a logger to use by the Client, default is NullLogger
    #
    def initialize(params = {})
      @mode = params[:mode]
      Strongkeep.mode_params(@mode) # validates the mode
      @client_key = Strongkeep::SigningKey.from_json(params[:client_key])
      raise ArgumentError, 'Client _private_ key is expected' unless @client_key.private?
      @logger = params[:logger] || Strongkeep::NullLogger.new
      @connection = Strongkeep::Connection.new(self, Strongkeep.mode_params(@mode))
    end

    # Returns true if the client is in live (production) mode
    #
    # @return [true,false]
    #
    def live?
      mode == :live
    end

    # Signs an arbitrary message with the client key
    #
    # @param message [String]
    # @return [String] a hex encoded signature of the message
    #
    def sign_message(message)
      client_key.sign(message)
    end

    # Tests the connection to the API and returns true if everything looks good
    #
    # @return [true,false]
    #
    def available?
      connection.get('test', a: rand(1000), b: rand(1000))
      connection.post('test', a: rand(1000), b: rand(1000))
      true
    rescue Strongkeep::Errors::ConnectionError => e
      logger.warn "API is not available: (#{e.class}) #{e}"
      false
    end

    # Returns the assets resource
    #
    # @return [Strongkeep::Assets]
    #
    def assets
      @assets ||= Strongkeep::Assets.new(self)
    end

    # Returns the accounts resource
    #
    # @return [Strongkeep::Accounts]
    #
    def accounts
      @accounts ||= Strongkeep::Accounts.new(self)
    end
  end # class Client
end # module Strongkeep
