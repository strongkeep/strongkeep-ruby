# frozen_string_literal: true

module Strongkeep
  module Utils
    # Converts a byte array to its hex representation, lowercase
    #
    # @param bytes [String]
    # @return [String] a hex encoded string
    #
    def b2h(bytes)
      raise ArgumentError, 'Invalid bytes, String is expected' unless bytes.is_a?(String)
      bytes.unpack('H*').first
    end

    # Converts a hex string into a byte array
    #
    # @param hex_string [String]
    # @return [String] a byte array
    #
    def h2b(hex_string)
      raise ArgumentError, 'Invalid hex_string, String is expected' unless hex_string.is_a?(String)
      [hex_string].pack('H*')
    end
  end # module Utils
end # module Strongkeep
