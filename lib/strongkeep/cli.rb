# frozen_string_literal: true

require 'thor'

module Strongkeep
  class CLI < Thor
    desc 'inspect FILENAME', 'Inspect a key in Strongkeep format'
    def inspect(filename)
      puts "filename: #{filename}"
      key = Strongkeep::SigningKey.from_json(File.read(filename))
      puts "private?: #{key.private? ? 'YES' : 'no'}"
      puts "   label: \"#{key.label}\""
      puts '  public:'
      puts key.to_public.to_json
    end

    desc 'generate', 'Generate a new key'
    option :randomness, type: :string, aliases: :r, default: '/dev/urandom', desc: 'Use as a source of random data'
    option :label, type: :string, aliases: :l, default: '', desc: 'Use a label for the new key'
    option :output, type: :string, aliases: :o, desc: 'Write new key to a file'
    def generate
      puts "     label: \"#{options[:label]}\""
      puts "randomness: #{options[:randomness]}"
      key = Strongkeep::SigningKey.generate_key(options[:label], options[:randomness])
      if options[:output]
        puts "    output: #{options[:output]}"
        File.open(options[:output], 'w') { |f| f.puts key.to_json }
      else
        puts '   private:'
        puts key.to_json
        puts
      end
      puts '    public:'
      puts key.to_public.to_json
    end

    map %w[--version -v] => :version
    desc 'version', 'Show version'
    def version
      puts Strongkeep::APP_NAME_VERSION
    end
  end # class CLI
end # module Strongkeep
