# frozen_string_literal: true

module Strongkeep
  # External dependency adapters, e.g. Bitcoin from `bitcoin-ruby`
  #
  module Ext
    # Synchronizes access to an object, executes the block exclusively
    #
    # Prevents other threads of the app to access the same object before
    # the block finishes. It is necessary for external libraries which are not
    # inherently threadsafe, like Bitcoin from `bitcoin-ruby`.
    #
    # Example:
    #   require 'bitcoin'
    #
    #   Strongkeep.exclusively_with(Bitcoin) do
    #     Bitcoin.network = :testnet
    #     Bitcoin.valid_address?('mpXwg4jMtRhuSpVq4xS3HFHmCmWp9NyGKt')
    #   end
    #
    #
    def exclusively_with(object, &_block)
      @exclusively_with_mutexes ||= {}
      @exclusively_with_mutexes[object.object_id] ||= Mutex.new
      @exclusively_with_mutexes[object.object_id].synchronize { yield }
    end
  end # module Ext
end # module Strongkeep
