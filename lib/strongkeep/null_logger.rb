# frozen_string_literal: true

module Strongkeep
  #
  # A Null Logger implements standard ::Logger interface,
  # but outputs nothing nowhere.
  #
  class NullLogger
    def initialize(*); end

    def level(*); end

    def level=(*); end

    def debug(*); end

    def info(*); end

    def warn(*); end

    def error(*); end

    def fatal(*); end
  end # class NullLogger
end # module Strongkeep
