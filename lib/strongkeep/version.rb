# frozen_string_literal: true

module Strongkeep
  VERSION = '0.4.0'
end
