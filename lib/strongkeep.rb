# frozen_string_literal: true

require_relative 'strongkeep/version'
require_relative 'strongkeep/null_logger'
require_relative 'strongkeep/errors'
require_relative 'strongkeep/utils'
require_relative 'strongkeep/signing_key'
require_relative 'strongkeep/client'
require_relative 'strongkeep/connection'
require_relative 'strongkeep/resource'
require_relative 'strongkeep/ext'
require_relative 'strongkeep/callback'

module Strongkeep
  extend Strongkeep::Ext

  APP_NAME_VERSION = "strongkeep v#{VERSION}"

  MODES = {
    # live or production mode
    live: {
      content_type: 'application/json',
      user_agent: "strongkeep-ruby/#{Strongkeep::VERSION} (live)",
      api_base_url: 'https://api.strongkeep.com/api/v1/',
      strongkeep_pub_keys: [
        '{"type":"ed25519","label":"Strongkeep Public @ 2018-08-22","pub_key":"6a1a4811cf2c4af816c8b9380d8842d079b1662cdf766453e6ebde0c0523fb85"}'
      ]
    },

    # test mode
    test: {
      content_type: 'application/json',
      user_agent: "strongkeep-ruby/#{Strongkeep::VERSION} (test)",
      api_base_url: 'https://test.cloud.strongkeep.com/api/v1/',
      strongkeep_pub_keys: [
        '{"type":"ed25519","label":"Strongkeep Public Test @ 2018-05-01","pub_key":"ad57c137bd2eff9f3c1f26315edd888fd5d01a6435d1d7712c9314ff481764a1"}'
      ]
    }
  }

  # Returns connection params for the given mode
  #
  # @param mode [String,Symbol]
  # @return [Hash]
  #
  def self.mode_params(mode)
    raise ArgumentError, 'Required mode value is missing' unless mode
    raise ArgumentError, "Invalid mode value: #{mode.inspect}" unless MODES[mode.to_sym]
    MODES[mode.to_sym]
  end
end # module Strongkeep
